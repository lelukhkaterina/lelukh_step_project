$(window).on('load', function (){
    //----------ourServices----Смена вкладок с текстом----------
    $(".OurServMenu").on("click", function (event) {
        let targetMenu = event.target;
        let dataIdMenu = event.target.dataset.servicetab;
        if($(targetMenu).hasClass("tabsOurServ")) {
            $(".OurServMenu .tabsOurServ").removeClass("tabsOurServActiv");
            $(targetMenu).addClass("tabsOurServActiv");
            $(".servicesTabsContent").addClass("notshow");
            $(`.${dataIdMenu}`).removeClass("notshow");
        }
    });

    //------------Клик по кнопке добавления картинок AmazingWork------------
    let counter=1;
    let buttonShImg= $("#buttonAmazingWork");
    buttonShImg.on("click", function (event) {
        buttonShImg.addClass("notshow");
        $(".circularGBlock").removeClass("notshow");
        let timerId = setTimeout(function () {
            buttonShImg.removeClass("notshow");
            $(".circularGBlock").addClass("notshow");
            if (counter===1) {
                $(".bl2").parent().removeClass("notshow");
                counter++;
            }
            else{
                $(".bl3").parent().removeClass("notshow");
                buttonShImg.addClass("notshow");
            }
        },2000);
        // clearTimeout(timerId);
     });


    // ------Клик по вкладкам, фильтр картинок---------------
    let img3Block=$(".ourAmazingWorkBlock img");
    $(".ourAmazingWorkList").on("click", function (event) {
        let targetMenu = event.target;
        let groupEl;
        let textDivMenu;
        if($(targetMenu).hasClass("tabsOurAmaz")) {
            textDivMenu = targetMenu.innerHTML;
            groupEl=textDivMenu.toLowerCase().split(" ").join("");
            $(".tabsOurAmaz").removeClass("tabsOurAmazActive");
            $(targetMenu).addClass("tabsOurAmazActive");
            img3Block.each(function(index, item){
                $(item).parent().removeClass("notshow");
                if (groupEl==="all"){
                    $(".bl2").parent().addClass("notshow");
                    $(".bl3").parent().addClass("notshow");
                    $("#buttonAmazingWork").removeClass("notshow");
                    counter=1;
                }
                else if (item.dataset.groupimg!==groupEl){
                    $("#buttonAmazingWork").addClass("notshow");
                    $(item).parent().addClass("notshow");
                }
            });
        }

    });
//------------------карусель--------------------------------
//    обрабатываем клик по мини картинке. Картинка должна поскочить, позеленеть,
//     а большая картинка должна изменится на нужную
    let idUser=1;
    let miniCollection=$(".ellipse1mini");
    let closestEl;

    function setEl(idUser, closestEl){
        miniCollection.removeClass("personMiniActive");
        $(closestEl).addClass("personMiniActive");
        $(".person").addClass("notshow");
        $(".person").animate({opacity: "hide"}, 0);
        $(`#${idUser}`).animate({opacity: "show"}, 500);
        $(`#${idUser}`).removeClass("notshow");
    }


    $('.smallImgBlock').on("click", function (event) {
        let target=event.target;
        let targetTagName=target.tagName.toLowerCase();
        if (targetTagName==="img"){
            let closestEl = $(target).closest(".ellipse1mini");
            idUser=parseInt(closestEl.data("idperson"));
            setEl(idUser, closestEl);
        }
    });


    //обработка клика по кнопке вправо
    let buttonRightEl=$(".buttonRight");
    buttonRightEl.on("click", function (event) {
        idUser++;
        if(idUser===miniCollection.length+1){
            idUser=1;
        }
        closestEl=miniCollection.filter(function (index,item) {
            return $(item).data("idperson")===idUser;
        });
        setEl(idUser, closestEl);
    });

    //обработка клика по кнопке влево
    let buttonLeftEl=$(".buttonLeft");
    buttonLeftEl.on("click", function (event) {
        idUser--;
        if(idUser===0){
            idUser=miniCollection.length;
        }
        closestEl=miniCollection.filter(function (index,item) {
            return $(item).data("idperson")===idUser;
        });
        setEl(idUser, closestEl);
    });

// --------------BestImg-----------------------------
//     $(document).ready(function(){
        $('#imgContainer').masonry({
// указываем элемент-контейнер в котором расположены блоки для динамической верстки
            itemSelector: '.itemImg',
// // указываем класс элемента являющегося блоком в нашей сетке
//             singleMode: false,
// // true - если у вас все блоки одинаковой ширины
//             isResizable: true,
// // перестраивает блоки при изменении размеров окна
//             isAnimated: true,
            columnWidth: 12,
            gutter: 2,
// // анимируем перестроение блоков
//             animationOptions: {
//                 queue: false,
//                 duration: 500
//             }
// // опции анимации - очередь и продолжительность анимации
//         });
    });

//------------Клик по кнопке добавления картинок  Best img------------
    let buttonBestImgEl= $("#buttonBestImg");
    buttonBestImgEl.on("click", function (event) {
        buttonBestImgEl.addClass("notshow");
        $(".circularGBlock").removeClass("notshow");
        let timerId = setTimeout(function () {
            // buttonBestImgEl.removeClass("notshow");
            $(".circularGBlock").addClass("notshow");
            $(".block2Best").parent().removeClass("notshow");
            $('#imgContainer').masonry({
                itemSelector: '.itemImg',
                columnWidth: 12,
                gutter: 2,
            });
            buttonBestImgEl.addClass("notshow");
        },2000);
        // clearTimeout(timerId);
    });




});